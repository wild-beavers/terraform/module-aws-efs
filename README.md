# Terraform EFS module

This module is not to be deployed directly.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.3 |
| aws | >= 5 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 5 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| kms | git@gitlab.com:wild-beavers/terraform/module-aws-kms | 0 |

## Resources

| Name | Type |
|------|------|
| [aws_efs_access_point.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/efs_access_point) | resource |
| [aws_efs_backup_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/efs_backup_policy) | resource |
| [aws_efs_file_system.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/efs_file_system) | resource |
| [aws_efs_file_system_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/efs_file_system_policy) | resource |
| [aws_efs_mount_target.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/efs_mount_target) | resource |
| [aws_iam_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_security_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.this_cidrs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_security_groups](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_ssm_parameter.this_efs_id](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_efs_mount_target.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/efs_mount_target) | data source |
| [aws_iam_group.policy_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_group) | data source |
| [aws_iam_policy_document.external](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.internal](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_subnet.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet) | data source |
| [aws_subnets.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnets) | data source |
| [aws_vpc.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| allowed\_cidrs | CIDRs allowed to access the EFS. By specifying this value, the module will create a new security group to attach to the EFS mount targets - with these CIDRS as targets - in addition to `var.security_group_ids`. | `list(string)` | `[]` | no |
| allowed\_security\_group\_ids | ID of the security groups allowed to access the EFS. By specifying this value, the module will create a new security group to attach to the EFS mount targets - with these security group IDs as targets - in addition to `var.security_group_ids`. | `list(string)` | `[]` | no |
| availability\_zone\_name | The AWS Availability Zone in which to create the file system. Used only if `var.one_zone_storage` is set to `true` | `string` | `""` | no |
| efs\_access\_point\_tags | Tags specific to the Access Points. Will be merged with `var.tags`. | `map(string)` | `{}` | no |
| efs\_access\_points | A map of objects that represent the access points to add to the EFS.<br/> Each access\_point object must have the format:<br/>  * posix\_user     (optional, object): Operating system user and group applied to all file system requests made using the access point.<br/>  * root\_directory (optional, object): Directory on the Amazon EFS file system that the access point provides access to.<br/>posix\_user must have the format:<br/>  * gid  (required, number): POSIX group ID used for all file system operations using this access point.<br/>  * secondary\_gids (optional, list(number)): Secondary POSIX group IDs used for all file system operations using this access point.<br/>  * uid (required, number): POSIX user ID used for all file system operations using this access point.<br/>root\_directory must have the format:<br/>  * path (optional, string):  Path on the EFS file system to expose as the root directory to NFS clients using the access point to access the EFS file system. A path can have up to four subdirectories. If the specified path does not exist, you are required to provide `creation_info`.<br/>  * creation\_info (optional, object): POSIX IDs and permissions to apply to the access point's Root Directory.<br/>creation\_info must have the format:<br/>  * owner\_gid (required, number): POSIX group ID to apply to the `root_directory`.<br/>  * owner\_uid (required, number): POSIX user ID to apply to the `root_directory`.<br/>  * permissions (required, number): POSIX permissions to apply to the `root_directory`, in the format of an octal number representing the file's mode bits. | <pre>map(object({<br/>    posix_user = optional(object({<br/>      gid            = number<br/>      secondary_gids = optional(list(number))<br/>      uid            = number<br/>    }))<br/>    root_directory = optional(object({<br/>      path = optional(string)<br/>      creation_info = optional(object({<br/>        owner_gid   = number<br/>        owner_uid   = number<br/>        permissions = string<br/>      }))<br/>    }))<br/>  }))</pre> | `{}` | no |
| efs\_backup\_policy\_enabled | Whether to enable EFS backup policy or not. | `bool` | `false` | no |
| efs\_file\_system\_policy\_bypass\_policy\_lockout\_safety\_check | A flag to indicate whether to bypass the `var.efs_file_system_policy` lockout safety check. | `bool` | `false` | no |
| efs\_tags | Tags specific for the EFS. Will be merged with `var.tags`. | `map(string)` | `{}` | no |
| iam\_policy\_create | Whether or not to create IAM policies for resources of this module. If `var.iam_user_enabled` is `true`, policies will be created anyway, ignoring this value. | `bool` | `false` | no |
| iam\_policy\_description | Description of the external policy to attach to the given principals. | `string` | `""` | no |
| iam\_policy\_entity\_arns | Restrict access to the key to the given IAM entities (roles or users). Wildcards are allowed. Allowed keys are `backup`, `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the permission scope for entities inside them. If the same entity is inside multiple scope, the least privileged scope takes precedence over the most privileged. | `map(map(string))` | `{}` | no |
| iam\_policy\_export\_actions | Whether or not to export IAM policies actions. A lightweight way to generate your own policies. | `bool` | `false` | no |
| iam\_policy\_export\_json | Whether or not to export IAM policies as JSON. | `bool` | `false` | no |
| iam\_policy\_external\_resources\_arns | ARNs of resources created outside this module to compose the external policies with. Along with internal resources created inside this module, the given ARNs will be merged into external IAM policies created by this module. Keys are free values. | `map(string)` | `{}` | no |
| iam\_policy\_external\_resources\_enabled | When `true`, user of this module is expected to set `var.iam_policy_external_resources_arns` with non null values. | `bool` | `false` | no |
| iam\_policy\_internal | A valid EFS policy JSON document to be applied to the EFS. Careful, this comes in complement of IAM entities passed to this module. | `string` | `"{}"` | no |
| iam\_policy\_name\_template | Template for the name of the policies to attach to the given IAM entities in `var.iam_policy_entity_arns`. “%s” must be somewhere inside the variable and will be replaced by the scope of the policy (`backup`, `ro`, `rw`, `rwd` or `full`). | `string` | `null` | no |
| iam\_policy\_path | Path in which to create the policies. | `string` | `"/"` | no |
| iam\_policy\_restrict\_by\_account\_ids | Restrict resources created by this module by the given account IDs. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains accounts wildcards. | `list(string)` | `[]` | no |
| iam\_policy\_sid\_prefix | Use a prefix for all `Sid` of all the policies created by this module. | `string` | `""` | no |
| iam\_policy\_source\_arns | Restrict access to the key to the given ARN sources. Wildcards are allowed. Allowed keys are `backup`, `ro`, `rw`, `rwd` or `full`, each specifying the expected level of access for entities inside. | `map(map(string))` | `{}` | no |
| iam\_policy\_source\_policy\_documents | JSON to concatenate to create the external policies. | `list(string)` | `[]` | no |
| iam\_policy\_tags | Tags specific to the IAM policies to create if toggle `var.iam_policy_create` is `true`. Will be merged `var.tags`. | `map(string)` | `{}` | no |
| kms\_iam\_admin\_arns | ARNs of the Administrators for KMS key of this module. If you set any policy, this is mandatory. This module will grant unrestricted access to these ARNs. Wildcards are authorized. Keys are free values. | `map(string)` | `{}` | no |
| kms\_iam\_policy\_entity\_arns | Restrict access to the KMS key to the given IAM entities (roles or users, groups are handled via `var.iam_policy_group_arns`). Wildcards are allowed. Allowed keys are `ro`, `rw`, `rwd` or `full`, each specifying the expected level of access for entities inside. | `map(map(string))` | `{}` | no |
| kms\_iam\_policy\_group\_arns | Restrict the access of the KMS key to the users of the given group ARNs. Used in addition of `var.iam_policy_entity_arns`. Wildcards are not accepted. | `map(map(string))` | `{}` | no |
| kms\_iam\_policy\_sid\_prefix | Prefix for all `Sid` of all the KMS policies created by this module. | `string` | `""` | no |
| kms\_key | Options of the KMS key to create.<br/>If given, `var.kms_key_id` will be ignored.<br/><br/>  * alias                   (required, string):      KMS key alias; Display name of the KMS key. Omit the `alias/`.<br/>  * policy\_json             (optional, string):      Additional policy to attach to the KMS key, merged with a baseline internal policy. Make sure to provide "var.iam\_admin\_arns" to avoid getting blocked.<br/>  * rotation\_enabled        (optional, bool, true):  Whether to automatically rotate the KMS key linked to the S3.<br/>  * deletion\_window\_in\_days (optional, number, 7):   The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between `7` and `30`, inclusive. If you do not specify a value, it defaults to `7`.<br/>  * tags                    (optional, map(string)): Tags to be used by the KMS key of this module. | <pre>object({<br/>    alias                   = string<br/>    policy_json             = optional(string, null)<br/>    rotation_enabled        = optional(bool, true)<br/>    deletion_window_in_days = optional(number, 7)<br/>    tags                    = optional(map(string), {})<br/>  })</pre> | `null` | no |
| kms\_key\_id | ARN or ID (or alias) of the AWS KMS key to be used to encrypt the resources data if “var.kms\_use\_external\_key” is set. | `string` | `""` | no |
| kms\_use\_external\_key | Whether to use KMS for EFS data encryption, using the key passed as `var.kms_key_id`. | `bool` | `false` | no |
| lifecycle\_policy | Lifecycle policy of the EFS<br/> * transition\_to\_archive               (optional, string): Indicates how long it takes to transition files to the archive storage class. Requires `transition_to_ia`, Elastic Throughput and General Purpose performance mode. Valid values: `AFTER_1_DAY`, `AFTER_7_DAYS`, `AFTER_14_DAYS`, `AFTER_30_DAYS`, `AFTER_60_DAYS`, `AFTER_90_DAYS`, `AFTER_180_DAYS`, `AFTER_270_DAYS`, or `AFTER_365_DAYS`<br/> * transition\_to\_ia                    (optional, string): Indicates how long it takes to transition files to the IA storage class. Valid values: `AFTER_1_DAY`, `AFTER_7_DAYS`, `AFTER_14_DAYS`, `AFTER_30_DAYS`, `AFTER_60_DAYS`, `AFTER_90_DAYS`, `AFTER_180_DAYS`, `AFTER_270_DAYS`, or `AFTER_365_DAYS`<br/> * transition\_to\_primary\_storage\_class (optional, string): Describes the policy used to transition a file from infrequent access storage to primary storage. Valid values: `AFTER_1_ACCESS`. | <pre>map(object({<br/>    transition_to_archive               = optional(string)<br/>    transition_to_ia                    = optional(string)<br/>    transition_to_primary_storage_class = optional(string)<br/>  }))</pre> | `{}` | no |
| name | Name of the EFS. | `string` | `"efs"` | no |
| one\_zone\_storage | Whether or not to create a file system that uses `One Zone` storage classes | `bool` | `false` | no |
| performance\_mode | The file system performance mode. Can be either ”generalPurpose” or “maxIO”. | `string` | `"generalPurpose"` | no |
| provisioned\_throughput\_in\_mibps | The throughput, measured in MiB/s, that you want to provision for the file system. Only applicable with `throughput_mode` set to “provisioned”. | `number` | `0` | no |
| require\_secure\_transport | Whether to require secure transport or not. | `bool` | `true` | no |
| security\_group\_ids | List of additional security group IDs for the EFS mount targets. Required if no `allowed_cidrs` nor `allowed_security_group_ids` is provided. | `list(string)` | `[]` | no |
| security\_group\_name | Name of the security group to be used by the EFS mount targets. Security group will be create ONLY IF `var.allowed_cidrs` or `var.allowed_security_group_ids` is NOT an empty list. | `string` | `"efs"` | no |
| security\_group\_tags | Additional tags specific for the security group for the EFS mount targets. Will be merged with `var.tags`. | `map(string)` | `{}` | no |
| ssm\_parameter\_enabled | Whether or not to create SSM Parameters containing EFS metadata. | `bool` | `false` | no |
| ssm\_parameter\_prefix | Prefix for the SSM Parameters created by this module. It should an absolute path without trailing slash (e.g /my/example/path). | `string` | `"/efs/module/default"` | no |
| ssm\_parameter\_tags | Tags specific for the SSM Parameters for the EFS. Will be merged with tags. | `map(string)` | `{}` | no |
| subnet\_ids | IDs of the subnet where the EFS should be made available. If none are specified, it will be deployed in the default vpc but WITHOUT mount targets. | `list(string)` | `[]` | no |
| tags | Tags to be shared among all resources of this module. | `map(string)` | `{}` | no |
| throughput\_mode | Throughput mode for the file system. Valid values: ”bursting”, “provisioned”, “elastic”. When using `provisioned`, also set the `provisioned_throughput_in_mibps` variable. | `string` | `"elastic"` | no |

## Outputs

| Name | Description |
|------|-------------|
| access\_points | List of created access points. For a complete list of all attributes present please refer to [aws\_efs\_access\_points](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/efs_access_point) |
| arn | Amazon Resource Name of the file system. |
| dns\_name | The DNS name for the filesystem. |
| iam\_policies | n/a |
| iam\_policy\_actions | n/a |
| iam\_policy\_jsons | n/a |
| id | The ID that identifies the file system (e.g. fs-ccfc0d65). |
| kms\_key | n/a |
| kms\_key\_iam\_actions | n/a |
| mount\_target\_ids | The IDs of the EFS mount targets. |
| mount\_target\_ips | List of mount target IPs |
| mount\_target\_network\_interface\_ids | The IDs of the network interfaces that Amazon EFS created when it created the mount targets. |
| security\_group\_arn | ARN of the security group used for the EFS. This output will be empty if the security groups IDs were passed as variables. |
| security\_group\_id | ID of the security group used for the EFS. This output will be empty if the security groups IDs were passed as variables. |
| security\_group\_rule\_ids | List of ID's of the security rules added to security group |
| ssm\_parameter\_arns | The ARNs of the SSM Parameters for the EFS. |
| ssm\_parameter\_names | The names of the SSM Parameters for the EFS. |
<!-- END_TF_DOCS -->
