locals {
  prefix     = "${random_string.start_letter.result}${random_string.this.result}"
  caller_arn = replace(replace(join("/", chunklist(split("/", data.aws_caller_identity.current.arn), 2)[0]), "sts", "iam"), "assumed-role", "role")
}

data "aws_caller_identity" "current" {}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

data "aws_vpc" "default" {
  default = true
}

data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_subnet" "default" {
  vpc_id            = data.aws_vpc.default.id
  availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_security_group" "example" {
  vpc_id = data.aws_vpc.default.id
  tags = {
    Name = "tftest-example-${local.prefix}"
  }
}

module "one_zone_storage_policy" {
  source = "../../"

  tags = {
    Name = "tftest${local.prefix}"
  }

  one_zone_storage       = true
  availability_zone_name = data.aws_availability_zones.available.names[0]
  subnet_ids             = [data.aws_subnet.default.id]

  name                = "tftest${local.prefix}"
  security_group_name = "tftest${local.prefix}"

  kms_key = {
    alias                   = "tftest${local.prefix}"
    rotation_enabled        = false
    deletion_window_in_days = 8
  }
  kms_iam_admin_arns = {
    call = local.caller_arn,
  }

  allowed_security_group_ids = [aws_security_group.example.id]

  efs_backup_policy_enabled = true

  lifecycle_policy = {
    primary = {
      transition_to_primary_storage_class = "AFTER_1_ACCESS"
    }
  }

  ssm_parameter_enabled    = false
  require_secure_transport = false

  providers = {
    aws.replica = aws.replica
  }
}
