locals {
  prefix     = "${random_string.start_letter.result}${random_string.this.result}"
  caller_arn = replace(replace(join("/", chunklist(split("/", data.aws_caller_identity.current.arn), 2)[0]), "sts", "iam"), "assumed-role", "role")
}

data "aws_caller_identity" "current" {}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnets" "default" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }

  filter {
    name   = "defaultForAz"
    values = ["true"]
  }
}

resource "aws_security_group" "with_external_kms_and_sgs" {
  name   = "tftest-${random_string.this.result}"
  vpc_id = data.aws_vpc.default.id
}

resource "aws_kms_key" "with_external_kms_and_sgs" {}

module "with_external_kms_and_sgs" {
  source = "../../"

  tags = {
    Name = "tftest${local.prefix}"
  }

  subnet_ids = [tolist(data.aws_subnets.default.ids)[0]]

  name = "tftest${local.prefix}"

  kms_use_external_key = true
  kms_key_id           = aws_kms_key.with_external_kms_and_sgs.arn

  security_group_ids = [aws_security_group.with_external_kms_and_sgs.id]

  require_secure_transport = false

  iam_policy_entity_arns = {
    ro = {
      foo = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/foo"
    }
    full = {
      caller = local.caller_arn
    }
  }

  providers = {
    aws.replica = aws.replica
  }
}
