#####
# EFS
#####

output "arn" {
  value = module.with_external_kms_and_sgs.arn
}

output "id" {
  value = module.with_external_kms_and_sgs.id
}

output "dns_name" {
  value = module.with_external_kms_and_sgs.dns_name
}

output "mount_target_network_interface_ids" {
  value = module.with_external_kms_and_sgs.mount_target_network_interface_ids
}

output "mount_target_ids" {
  value = module.with_external_kms_and_sgs.mount_target_ids
}

output "mount_target_ips" {
  value = module.with_external_kms_and_sgs.mount_target_ips
}

#####
# KMS
#####

output "kms_key" {
  value = module.with_external_kms_and_sgs.kms_key
}

output "kms_key_iam_actions" {
  value = module.with_external_kms_and_sgs.kms_key_iam_actions
}

#####
# SSM Parameter
#####

output "ssm_parameter_arns" {
  value = module.with_external_kms_and_sgs.ssm_parameter_arns
}

output "ssm_parameter_names" {
  value = module.with_external_kms_and_sgs.ssm_parameter_names
}

#####
# Security group
#####

output "security_group_id" {
  value = module.with_external_kms_and_sgs.security_group_id
}

output "security_group_arn" {
  value = module.with_external_kms_and_sgs.security_group_arn
}

####
# Access Points
####

output "access_points" {
  value = module.with_external_kms_and_sgs.access_points
}
