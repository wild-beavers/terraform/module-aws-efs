locals {
  prefix     = "${random_string.start_letter.result}${random_string.this.result}"
  caller_arn = replace(replace(join("/", chunklist(split("/", data.aws_caller_identity.current.arn), 2)[0]), "sts", "iam"), "assumed-role", "role")
}

data "aws_caller_identity" "current" {}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnets" "default" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }

  filter {
    name   = "defaultForAz"
    values = ["true"]
  }
}

data "aws_iam_policy_document" "sts_ec2" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
    effect = "Allow"
  }
}

resource "aws_iam_role" "test" {
  assume_role_policy = data.aws_iam_policy_document.sts_ec2.json
  name               = "tftest${local.prefix}"
}

module "standard" {
  source = "../../"

  tags = {
    Name = "tftest${local.prefix}"
  }

  subnet_ids = data.aws_subnets.default.ids

  name                = "tftest${local.prefix}"
  security_group_name = "tftest${local.prefix}"

  kms_key = {
    alias = "tftest${local.prefix}"
  }

  kms_iam_admin_arns = {
    call = local.caller_arn,
  }

  kms_iam_policy_entity_arns = {
    rw = {
      0 = aws_iam_role.test.arn
    }
  }

  allowed_cidrs = ["10.0.0.0/8"]

  ssm_parameter_enabled = true
  ssm_parameter_prefix  = "/param/tftest${local.prefix}"

  efs_access_points = {
    main = {
      posix_user = {
        gid            = 1001
        uid            = 1001
        secondary_gids = [1001]
      }
      root_directory = {
        path = "/somefolder/somefolder2"
        creation_info = {
          owner_gid   = 1001
          owner_uid   = 1001
          permissions = "755"
        }
      }
    }
  }

  efs_access_point_tags = {
    efs-access-point-tag = "some value"
  }

  iam_policy_entity_arns = {
    ro = {
      1 = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/foo"
    }
    full = {
      caller = local.caller_arn
    }
  }
  iam_policy_source_arns = {
    rwd = {
      2 = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/bar"
    }
    ro = {
      3 = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/baz"
    }
  }
  providers = {
    aws.replica = aws.replica
  }
}
