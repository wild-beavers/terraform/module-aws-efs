#####
# EFS
#####

output "arn" {
  value = module.standard.arn
}

output "id" {
  value = module.standard.id
}

output "dns_name" {
  value = module.standard.dns_name
}

output "mount_target_network_interface_ids" {
  value = module.standard.mount_target_network_interface_ids
}

output "mount_target_ids" {
  value = module.standard.mount_target_ids
}

output "mount_target_ips" {
  value = module.standard.mount_target_ips
}

#####
# KMS
#####

output "kms_key" {
  value = module.standard.kms_key
}

output "kms_key_iam_actions" {
  value = module.standard.kms_key_iam_actions
}

#####
# SSM Parameter
#####

output "ssm_parameter_arns" {
  value = module.standard.ssm_parameter_arns
}

output "ssm_parameter_names" {
  value = module.standard.ssm_parameter_names
}

#####
# Security group
#####

output "security_group_id" {
  value = module.standard.security_group_id
}

output "security_group_arn" {
  value = module.standard.security_group_arn
}

####
# Access Points
####

output "access_points" {
  value = module.standard.access_points
}
