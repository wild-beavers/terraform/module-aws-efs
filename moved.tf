# TO BE REMOVE ONCE VERSION 7 APPEAR IN THE CHANGELOG

moved {
  from = aws_kms_key.this[0]
  to   = module.kms["0"].aws_kms_key.this["0"]
}

moved {
  from = aws_kms_alias.this[0]
  to   = module.kms["0"].aws_kms_alias.this["0"]
}

# TO BE REMOVE ONCE VERSION 7 APPEAR IN THE CHANGELOG
