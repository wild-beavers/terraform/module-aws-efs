## 7.2.0

- feat: adds `backup` IAM scope
- tech: use new pre-commit hooks best practices

## 7.1.0

- feat: set `elastic` as default value for `throughput_mode`

## 7.0.1

- fix: (IAM) wrong input type on IAM policy

## 7.0.0

- feat: (BREAKING) remove `var.enable_efs_file_system_policy` and `var.efs_file_system_policy_json`. Use `var.iam_*` instead
- feat: (BREAKING) rename `var.enable_efs_backup_policy` to `var.efs_backup_policy_enabled` variable
- feat: (BREAKING) `var.lifecycle_policy` is now a map of object instead of flat object
- feat: `var.lifecycle_policy` now support the key `transition_to_archive`
- feat: adds `var.require_secure_transport` variable to force TLS connection only to the EFS
- feat: adds EFS internal policy creation controlled by `var.iam_*` variables. This also adds `iam_policy_jsons`, `iam_policy_actions` and `iam_policies` outputs
- refactor: adds `nullable = false` on some variables to simplify code
- chore: bump pre-commit hooks

## 6.0.0

- feat: (BREAKING) the module now requires `replica` AWS provider
- refactor: (BREAKING) removes following outputs in favor of “kms_key” output:
   - kms_key_arn
   - kms_key_id
   - kms_alias_arn
- refactor: uses external KMS module internally, instead of managing KMS resources
- feat: add a more restrictive KMS policy. This introduces “var.kms_iam_policy_sid_prefix”, “var.kms_iam_admin_arns”, “var.kms_iam_policy_entity_arns” and “var.kms_iam_policy_group_arns”
- feat: add “kms_key_iam_actions” output when “var.kms_use_external_key” is not set
- refactor: (BREAKING) replace “var.kms_key_create” and “var.kms_key_alias_name” by “var.kms_key”
- test: rename `standard` test to `default` and comply with Wildbeaver standards
- chore: bump pre-commit hooks
- chore: update changelog to comply to Wildbeaver standards
- chore: adds `origin` tag

## 5.0.1

- fix: removed deprecated ssm_parameter overwrite
- maintenance: use aws_provider version >=5

## 5.0.0

- maintenance: (BREAKING) pin minimal AWS provider version to `4+`
- maintenance: use `aws_subnets` data source to replace the deprecated `aws_subnet_ids`
- refactor: (BREAKING) pin minimal Terraform to `1.3`
- test: missing outputs
- test: remove Jenkinsfile
- chore: bump pre-commit hooks
- chore: update LICENSE to add the current year

## 4.1.1

- maintenance: fix TF lint issue
- chore: bump pre-commit hooks
- test: adds gitlab ci

## 4.1.0

- feat: Add `access points` creation for EFS

## 4.0.0

- feat: (BREAKING) Handle lifecycle with all possible values
- doc: improves security group variables documentation and validation

## 3.1.0

- feat: mount target ip output
- chore: bump pre-commit hooks
- chore: add LICENSE file

## 3.0.0

- feat: add one zone storage
- feat: add backup policy
- feat: add efs file system policy
- feat (BREAKING): rename efs prefix on aws_efs resources output
- feat: add security group arn output
- chore: bump pre-commit hook
- chore: Remove author file
- tech(BREAKING): Update to Terraform 1.x
- tech(BREAKING): Remove `enabled` variable

## 2.1.2

- chore: fix pre-commit

## 2.1.1

- fix: Do not depend on default vpc

## 2.1.0

- Fix pre-commit
- feat/ add more outputs

## 2.0.0

- Fixing some stuff
- Trying stuff out
- Fix local not calculated ahead of time
- Fix
- Fixed small problems
- Adapted example to fit new version
- Adapted module to better fit needs

## 1.0.0

- Fix last example
- Fixing 0.12 upgrade command malformation
- Fix workaround for AWS provider bug
- Fixed another example
- Fix for subnet id provider issue
- Mix up
- Fixed typo
- Trying to fix for terraofrm 0.11
- Add workaround
- Set AWS provider version higher
- Change Jenkinsfile to use latest version of terraform
- Upgrade to terraform 0.12

## 0.1.1

- feat: fixes the code
- test: test
- test: test
- test: test
- tech: updates pipeline Terraform version
- test: test the fix
- test: tests new pipeline

## 0.1.0

- fix: fixes Terraform 0.11 inabilities
- test: fixes testrs
- fix: returns secu group id not rule id
- fix: fixes security group creation condition
- refactor: removes efs mount target names
- test: fixes KMS key alias issue
- feat: adds outputs for examples
- refactor: removes random string from disabled test
- fix: makes sure apply works even without subnets
- test: changes SSM parameter prefix
- feat: adds outputs for main module
- refactor: removes uneeded vpc_id
- refactor: makes subnet_ids facultative
- test: adds disables test
- test: adds standard test
- feat: adds data.tf
- feat: creates main EFS module
- test: adds Jenkinsfile
- tech: adds pre-commit config
- tech: adds gitignore
- Initial commit
