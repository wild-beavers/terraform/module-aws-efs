data "aws_vpc" "default" {
  count = 0 == length(var.subnet_ids) ? 1 : 0

  default = true
}

data "aws_subnets" "default" {
  count = 0 == length(var.subnet_ids) ? 1 : 0

  filter {
    name   = "vpc-id"
    values = [try(data.aws_vpc.default[0].id, "")]
  }
}

data "aws_subnet" "default" {
  id = try(data.aws_subnets.default[0].ids, var.subnet_ids[0])
}

data "aws_efs_mount_target" "this" {
  count = length(var.subnet_ids) > 0 ? length(var.subnet_ids) : 0

  mount_target_id = aws_efs_mount_target.this[count.index].id
}
