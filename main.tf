locals {
  tags = {
    origin     = "gitlab.com/wild-beavers/terraform/module-aws-efs"
    managed-by = "terraform"
  }
}

#####
# EFS
#####

resource "aws_efs_file_system" "this" {
  availability_zone_name = var.one_zone_storage ? var.availability_zone_name : null

  encrypted  = true
  kms_key_id = local.kms_should_create ? module.kms["0"].kms_key["0"].arn : var.kms_key_id

  provisioned_throughput_in_mibps = var.provisioned_throughput_in_mibps
  performance_mode                = var.performance_mode
  throughput_mode                 = var.throughput_mode

  dynamic "lifecycle_policy" {
    for_each = var.lifecycle_policy

    content {
      transition_to_ia                    = lifecycle_policy.value.transition_to_ia
      transition_to_primary_storage_class = lifecycle_policy.value.transition_to_primary_storage_class
    }
  }

  tags = merge(
    {
      Name = var.name
    },
    local.tags,
    var.tags,
    var.efs_tags,
  )

  lifecycle {
    postcondition {
      condition     = !(!local.kms_should_create && (var.kms_key_id == null || var.kms_key_id == ""))
      error_message = "“var.kms_key_arn” must be set when “var.kms_key_creation_enabled” is false"
    }
  }
}

resource "aws_efs_mount_target" "this" {
  count = length(var.subnet_ids) > 0 ? length(var.subnet_ids) : 0

  file_system_id = aws_efs_file_system.this.id
  subnet_id      = var.subnet_ids[count.index]

  security_groups = concat(try([aws_security_group.this[0].id], []), var.security_group_ids)
}

resource "aws_efs_backup_policy" "this" {
  file_system_id = aws_efs_file_system.this.id
  backup_policy {
    status = var.efs_backup_policy_enabled ? "ENABLED" : "DISABLED"
  }
}

#####
# SSM Parameter
#####

resource "aws_ssm_parameter" "this_efs_id" {
  count = var.ssm_parameter_enabled ? 1 : 0

  name        = "${var.ssm_parameter_prefix}/efs/id"
  description = "ID of the ${var.name} EFS."
  type        = "String"
  value       = aws_efs_file_system.this.id

  tags = merge(
    local.tags,
    var.tags,
    var.ssm_parameter_tags,
  )
}

#####
# Security group
#####

resource "aws_security_group" "this" {
  count = (length(var.allowed_cidrs) > 0 || length(var.allowed_security_group_ids) > 0) ? 1 : 0

  name        = var.security_group_name
  description = "Security group for ${var.name} EFS."
  vpc_id      = data.aws_subnet.default.vpc_id

  tags = merge(
    {
      "Name" = var.security_group_name
    },
    local.tags,
    var.tags,
    var.security_group_tags,
  )
}

resource "aws_security_group_rule" "this_cidrs" {
  count = (length(var.allowed_cidrs) > 0 || length(var.allowed_security_group_ids) > 0) ? length(var.allowed_cidrs) : 0

  security_group_id = try(aws_security_group.this[0].id, "")

  type        = "ingress"
  from_port   = 2049
  to_port     = 2049
  protocol    = "tcp"
  description = "NFS from ${element(var.allowed_cidrs, count.index)}."
  cidr_blocks = [var.allowed_cidrs[count.index]]
}

resource "aws_security_group_rule" "this_security_groups" {
  count = (length(var.allowed_cidrs) > 0 || length(var.allowed_security_group_ids) > 0) ? length(var.allowed_security_group_ids) : 0

  security_group_id = try(aws_security_group.this[0].id, "")

  type                     = "ingress"
  from_port                = 2049
  to_port                  = 2049
  protocol                 = "tcp"
  description              = "NFS from ${element(var.allowed_security_group_ids, count.index)}."
  source_security_group_id = var.allowed_security_group_ids[count.index]
}

####
# Access Points
####

resource "aws_efs_access_point" "this" {
  for_each = var.efs_access_points

  file_system_id = aws_efs_file_system.this.id

  dynamic "posix_user" {
    for_each = each.value.posix_user != null ? [1] : []

    content {
      gid            = each.value.posix_user.gid
      secondary_gids = each.value.posix_user.secondary_gids
      uid            = each.value.posix_user.uid
    }
  }

  dynamic "root_directory" {
    for_each = each.value.root_directory != null ? [1] : []

    content {
      path = each.value.root_directory.path

      dynamic "creation_info" {
        for_each = each.value.root_directory.creation_info != null ? [1] : []

        content {
          owner_gid   = each.value.root_directory.creation_info.owner_gid
          owner_uid   = each.value.root_directory.creation_info.owner_uid
          permissions = each.value.root_directory.creation_info.permissions
        }
      }
    }
  }

  tags = merge(
    local.tags,
    var.tags,
    var.efs_access_point_tags,
  )
}

#####
# Policy
#####

locals {
  iam_ro_actions = [
    "elasticfilesystem:ClientMount",
    "elasticfilesystem:ClientRootAccess",
    "elasticfilesystem:DescribeMountTargets",
  ]
  iam_rw_actions = concat(
    local.iam_ro_actions,
    [
      "elasticfilesystem:ClientWrite",
    ]
  )
  iam_rwd_actions = local.iam_rw_actions
  iam_audit_actions = [
    "elasticfilesystem:Describe*",
    "elasticfilesystem:List*",
  ]
  iam_full_actions = [
    "elasticfilesystem:*",
  ]
  iam_backup_actions = [
    "elasticfilesystem:backup",
    "elasticfilesystem:DescribeTags"
  ]

  iam_scoped_actions = {
    backup = local.iam_backup_actions

    ro    = local.iam_ro_actions
    rw    = local.iam_rw_actions
    rwd   = local.iam_rwd_actions
    audit = local.iam_audit_actions
    full  = local.iam_full_actions
  }

  efs_policy_enabled = length(merge(var.iam_policy_entity_arns, var.iam_policy_source_arns)) > 0 || var.require_secure_transport || length(var.iam_policy_restrict_by_account_ids) > 0 || var.iam_policy_external_resources_enabled
}

data "aws_iam_policy_document" "internal" {
  for_each = local.efs_policy_enabled ? { 0 = 0 } : {}

  source_policy_documents = [var.iam_policy_internal]

  dynamic "statement" {
    for_each = length(local.iam_entities_all_scopes) > 0 ? { 0 = 0 } : {}

    content {
      sid     = "${try(chomp(var.iam_policy_sid_prefix), "")}Whitelist"
      effect  = "Deny"
      actions = ["elasticfilesystem:*"]
      resources = [
        aws_efs_file_system.this.arn
      ]
      principals {
        type        = "AWS"
        identifiers = ["*"]
      }
      condition {
        test     = "StringNotLike"
        values   = sort(compact(local.iam_policy_entity_all_arns))
        variable = "aws:principalArn"
      }
      dynamic "condition" {
        for_each = length(local.iam_policy_source_all_arns) > 0 ? [1] : []

        content {
          test     = "StringNotLike"
          values   = sort(compact(local.iam_policy_source_all_arns))
          variable = "aws:sourceArn"
        }
      }
    }
  }

  dynamic "statement" {
    for_each = setsubtract(local.iam_entities_all_scopes, ["full"])

    content {
      sid         = "${try(chomp(var.iam_policy_sid_prefix), "")}RestrictActions${upper(statement.key)}"
      effect      = "Deny"
      not_actions = local.iam_scoped_actions[statement.key]
      resources = [
        aws_efs_file_system.this.arn
      ]
      principals {
        type        = "AWS"
        identifiers = ["*"]
      }
      dynamic "condition" {
        for_each = contains(keys(local.iam_policy_entity_scoped_arns), statement.key) && length(lookup(local.iam_policy_entity_scoped_arns, statement.key, {})) > 0 ? [1] : []

        content {
          test     = "StringLike"
          values   = sort(distinct(compact(values(local.iam_policy_entity_scoped_arns[statement.key]))))
          variable = "aws:principalArn"
        }
      }
      dynamic "condition" {
        for_each = contains(keys(local.iam_policy_source_arns), statement.key) && length(lookup(local.iam_policy_source_arns, statement.key, {})) > 0 ? [1] : []

        content {
          test     = "StringLike"
          values   = sort(distinct(compact(local.iam_policy_source_arns[statement.key])))
          variable = "aws:sourceArn"
        }
      }

      // While seemingly useless, conditions below are necessary when wildcards are used.
      // A wildcard can conflict with the "full"-scoped entities, therefore blocking administrative permissions for the EFS
      dynamic "condition" {
        for_each = contains(keys(local.iam_policy_source_arns), "full") && length(lookup(local.iam_policy_source_arns, "full", {})) > 0 ? [1] : []

        content {
          test     = "StringNotLike"
          values   = sort(distinct(compact(local.iam_policy_source_arns["full"])))
          variable = "aws:sourceArn"
        }
      }
      dynamic "condition" {
        for_each = contains(keys(local.iam_policy_entity_scoped_arns), "full") && length(lookup(local.iam_policy_entity_scoped_arns, "full", {})) > 0 ? [1] : []

        content {
          test     = "StringNotLike"
          values   = sort(distinct(compact(values(local.iam_policy_entity_scoped_arns["full"]))))
          variable = "aws:principalArn"
        }
      }
    }
  }

  dynamic "statement" {
    for_each = length(var.iam_policy_restrict_by_account_ids) > 0 ? [1] : []

    content {
      sid     = "${try(chomp(var.iam_policy_sid_prefix), "")}AccountsRestriction"
      effect  = "Deny"
      actions = ["elasticfilesystem:*"]
      resources = [
        aws_efs_file_system.this.arn
      ]
      principals {
        type        = "AWS"
        identifiers = ["*"]
      }
      condition {
        test     = "StringNotEqualsIfExists"
        values   = sort(distinct(compact(var.iam_policy_restrict_by_account_ids)))
        variable = "aws:PrincipalAccount"
      }
      condition {
        test     = "StringNotEqualsIfExists"
        values   = sort(compact(var.iam_policy_restrict_by_account_ids))
        variable = "aws:SourceAccount"
      }
    }
  }

  dynamic "statement" {
    for_each = var.require_secure_transport ? { 0 = 0 } : {}

    content {
      sid    = "RequireSecureTransport"
      effect = "Deny"
      actions = [
        "elasticfilesystem:*",
      ]
      resources = [
        aws_efs_file_system.this.arn
      ]
      principals {
        type        = "AWS"
        identifiers = ["*"]
      }
      condition {
        test     = "Bool"
        variable = "aws:SecureTransport"
        values   = ["false"]
      }
    }
  }

  lifecycle {
    precondition {
      condition     = !(length(lookup(local.iam_policy_entity_scoped_arns, "full", {})) == 0 && length(keys(local.iam_policy_entity_scoped_arns)) > 0)
      error_message = "You need to set at least one “var.iam_policy_entity_arns.full” to avoid EFS becoming inaccessible."
    }
    precondition {
      condition = (
        length(setintersection(values(lookup(local.iam_policy_entity_scoped_arns, "full", {})), values(lookup(local.iam_policy_entity_scoped_arns, "backup", {})))) == 0 &&
        length(setintersection(values(lookup(local.iam_policy_entity_scoped_arns, "full", {})), values(lookup(local.iam_policy_entity_scoped_arns, "ro", {})))) == 0 &&
        length(setintersection(values(lookup(local.iam_policy_entity_scoped_arns, "full", {})), values(lookup(local.iam_policy_entity_scoped_arns, "rw", {})))) == 0 &&
        length(setintersection(values(lookup(local.iam_policy_entity_scoped_arns, "full", {})), values(lookup(local.iam_policy_entity_scoped_arns, "rwd", {})))) == 0 &&
        length(setintersection(values(lookup(local.iam_policy_entity_scoped_arns, "full", {})), values(lookup(local.iam_policy_entity_scoped_arns, "audit", {})))) == 0
      )
      error_message = <<-ERROR
At least one administrator (scoped inside 'full') is also inside a non-admin scope (e.g. 'ro', 'rw').
Because the least permissive scope takes precedence over the most permissive, this can break administrator permissions and thus can lead to the EFS being unmanageable.
ERROR
    }
    precondition {
      condition     = !contains([for key, entities in local.iam_policy_entity_scoped_arns : (length(compact(values(entities))) != 0)], false)
      error_message = <<-ERROR
It seems at least one scope (e.g. "ro", "rw") was set but contains no entities.
You may have passed a scope with a `null` or empty value.
This raises this error to prevent internal policy from becoming unmanageable.
ERROR
    }
  }
}

resource "aws_efs_file_system_policy" "this" {
  count = local.efs_policy_enabled ? 1 : 0

  file_system_id                     = aws_efs_file_system.this.id
  bypass_policy_lockout_safety_check = var.efs_file_system_policy_bypass_policy_lockout_safety_check
  policy                             = data.aws_iam_policy_document.internal["0"].json
}
