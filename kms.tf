####
# Variables
####

variable "kms_use_external_key" {
  description = "Whether to use KMS for EFS data encryption, using the key passed as `var.kms_key_id`."
  type        = bool
  default     = false
  nullable    = false
}

variable "kms_key_id" {
  description = "ARN or ID (or alias) of the AWS KMS key to be used to encrypt the resources data if “var.kms_use_external_key” is set."
  type        = string
  default     = ""
  nullable    = false
}

variable "kms_key" {
  description = <<-DOCUMENTATION
Options of the KMS key to create.
If given, `var.kms_key_id` will be ignored.

  * alias                   (required, string):      KMS key alias; Display name of the KMS key. Omit the `alias/`.
  * policy_json             (optional, string):      Additional policy to attach to the KMS key, merged with a baseline internal policy. Make sure to provide "var.iam_admin_arns" to avoid getting blocked.
  * rotation_enabled        (optional, bool, true):  Whether to automatically rotate the KMS key linked to the S3.
  * deletion_window_in_days (optional, number, 7):   The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between `7` and `30`, inclusive. If you do not specify a value, it defaults to `7`.
  * tags                    (optional, map(string)): Tags to be used by the KMS key of this module.
DOCUMENTATION
  type = object({
    alias                   = string
    policy_json             = optional(string, null)
    rotation_enabled        = optional(bool, true)
    deletion_window_in_days = optional(number, 7)
    tags                    = optional(map(string), {})
  })
  default = null
}

variable "kms_iam_policy_sid_prefix" {
  description = "Prefix for all `Sid` of all the KMS policies created by this module."
  type        = string
  default     = ""
  nullable    = false
}

variable "kms_iam_admin_arns" {
  description = "ARNs of the Administrators for KMS key of this module. If you set any policy, this is mandatory. This module will grant unrestricted access to these ARNs. Wildcards are authorized. Keys are free values."
  type        = map(string)
  default     = {}
  nullable    = false

  validation {
    condition = ((!contains([
      for arn in var.kms_iam_admin_arns :
      (
        (arn == null || can(regex("^arn:aws(-us-gov|-cn)?:iam:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws):(federated-user|user|role)/[a-zA-Z0-9+=\\*,\\./@-]+$", arn)))
      )
    ], false)))
    error_message = "One or more “var.kms_iam_admin_arns” are invalid. Check the requirements in the variables.tf file."
  }
}

variable "kms_iam_policy_entity_arns" {
  description = "Restrict access to the KMS key to the given IAM entities (roles or users, groups are handled via `var.iam_policy_group_arns`). Wildcards are allowed. Allowed keys are `ro`, `rw`, `rwd` or `full`, each specifying the expected level of access for entities inside."
  type        = map(map(string))
  default     = {}
  nullable    = false

  validation {
    condition = ((!contains([
      for key, arns in var.kms_iam_policy_entity_arns :
      (
        contains(["ro", "rw", "rwd", "full"], key) &&
        (
          !contains([
            for arn in values(arns) :
            (
              arn == null || can(regex("^arn:aws(-us-gov|-cn)?:(iam|sts):([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws|\\*):(assumed-role|role|user|federated-user)/[a-zA-Z0-9+=,_:\\*\\./@{}'\\$\\?-]+$", arn))
            )
          ], false)
        )
      )
    ], false)))
    error_message = "One or more “var.kms_iam_policy_entity_arns” are invalid."
  }
}

variable "kms_iam_policy_group_arns" {
  description = "Restrict the access of the KMS key to the users of the given group ARNs. Used in addition of `var.iam_policy_entity_arns`. Wildcards are not accepted."
  type        = map(map(string))
  default     = {}
  nullable    = false

  validation {
    condition = ((!contains([
      for key, arns in var.kms_iam_policy_group_arns :
      (
        contains(["ro", "rw", "rwd", "full"], key) &&
        !contains([
          for key, arn in arns :
          (
            can(regex("^arn:aws(-us-gov|-cn)?:iam:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws):group/[a-zA-Z0-9+=,\\./@-]+$", arn))
          )
        ], false)
      )
    ], false)))
    error_message = "One or more “var.kms_iam_policy_group_arns” are invalid. Check the requirements in the variables.tf file."
  }
}

####
# Locals
####

locals {
  kms_should_create = var.kms_key != null && !var.kms_use_external_key
  kms_iam_policy_group_arns = merge([
    for scope, entities in var.kms_iam_policy_group_arns :
    { for key, arn in entities :
      "${scope}${key}" => arn
    }
  ]...)
  kms_iam_policy_group_user_arns = {
    for scope, entities in var.kms_iam_policy_group_arns :
    scope => flatten([
      for key, arn in entities :
      [
        for i, user in data.aws_iam_group.policy_group["${scope}${key}"].users :
        user.arn
      ]
    ])
  }
  kms_iam_policy_entity_arns = {
    for scope in distinct(concat(keys(var.kms_iam_policy_entity_arns), keys(var.kms_iam_policy_group_arns))) :
    scope => concat(values(lookup(var.kms_iam_policy_entity_arns, scope, {})), lookup(local.kms_iam_policy_group_user_arns, scope, []))
  }
  kms_iam_ro_actions = [
    "kms:Decrypt",
  ]
  kms_iam_rw_actions = concat(
    [
      "kms:GenerateDataKey",
    ],
    local.kms_iam_ro_actions
  )
  kms_iam_actions = {
    ro   = local.kms_iam_ro_actions
    rw   = local.kms_iam_rw_actions
    rwd  = local.kms_iam_rw_actions
    full = local.kms_iam_rw_actions
  }
}

data "aws_iam_group" "policy_group" {
  for_each = local.kms_iam_policy_group_arns

  group_name = split(":group/", each.value)[1]
}

module "kms" {
  source = "git@gitlab.com:wild-beavers/terraform/module-aws-kms?ref=0"

  for_each = local.kms_should_create ? { 0 = var.kms_key } : {}

  kms_keys = {
    0 = {
      alias                   = each.value.alias
      deletion_window_in_days = each.value.deletion_window_in_days
      description             = "KMS Key for ${each.value.alias} EFS encryption."
      principal_actions       = local.kms_iam_actions
      policy_json             = each.value.policy_json
      rotation_enabled        = each.value.rotation_enabled
      service_principal       = "elasticfilesystem"
      service_actions = concat(
        // https://docs.aws.amazon.com/efs/latest/ug/encryption-at-rest.html
        [
          "kms:Encrypt",
          "kms:DescribeKey"
        ],
        local.kms_iam_rw_actions
      )

      tags = merge(
        local.tags,
        var.tags,
        each.value.tags,
      )
    }
  }

  iam_policy_create      = false
  iam_policy_export_json = false
  iam_admin_arns         = var.kms_iam_admin_arns
  iam_policy_entity_arns = local.kms_iam_policy_entity_arns
  iam_policy_sid_prefix  = var.kms_iam_policy_sid_prefix

  providers = {
    aws.replica = aws.replica
  }
}

####
# Outputs
####

output "kms_key" {
  value = local.kms_should_create ? module.kms["0"].kms_key["0"] : null
}

output "kms_key_iam_actions" {
  value = local.kms_should_create ? local.kms_iam_actions : null
}
