#####
# EFS
#####

output "arn" {
  description = "Amazon Resource Name of the file system."
  value       = aws_efs_file_system.this.arn
}

output "id" {
  description = "The ID that identifies the file system (e.g. fs-ccfc0d65)."
  value       = aws_efs_file_system.this.id
}

output "dns_name" {
  description = "The DNS name for the filesystem."
  value       = aws_efs_file_system.this.dns_name
}

output "mount_target_network_interface_ids" {
  description = "The IDs of the network interfaces that Amazon EFS created when it created the mount targets."
  value       = [for mount_target in data.aws_efs_mount_target.this : mount_target.network_interface_id]
}

output "mount_target_ids" {
  description = "The IDs of the EFS mount targets."
  value       = [for mount_target in data.aws_efs_mount_target.this : mount_target.id]
}

output "mount_target_ips" {
  description = "List of mount target IPs"
  value       = [for mount_target in data.aws_efs_mount_target.this : mount_target.ip_address]
}

#####
# SSM Parameter
#####

output "ssm_parameter_arns" {
  description = "The ARNs of the SSM Parameters for the EFS."
  value = compact(
    [
      try(aws_ssm_parameter.this_efs_id[0].arn, ""),
    ],
  )
}

output "ssm_parameter_names" {
  description = "The names of the SSM Parameters for the EFS."
  value = compact(
    [
      try(aws_ssm_parameter.this_efs_id[0].name, ""),
    ],
  )
}

#####
# Security group
#####

output "security_group_id" {
  description = "ID of the security group used for the EFS. This output will be empty if the security groups IDs were passed as variables."
  value       = try(aws_security_group.this[0].id, "")
}

output "security_group_arn" {
  description = "ARN of the security group used for the EFS. This output will be empty if the security groups IDs were passed as variables."
  value       = try(aws_security_group.this[0].arn, "")
}

output "security_group_rule_ids" {
  description = "List of ID's of the security rules added to security group"
  value       = concat([for rule in aws_security_group_rule.this_cidrs : rule.id], [for rule in aws_security_group_rule.this_security_groups : rule.id])
}

####
# Access Points
####

output "access_points" {
  description = "List of created access points. For a complete list of all attributes present please refer to [aws_efs_access_points](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/efs_access_point)"
  value       = aws_efs_access_point.this
}
