variable "iam_policy_create" {
  description = "Whether or not to create IAM policies for resources of this module. If `var.iam_user_enabled` is `true`, policies will be created anyway, ignoring this value."
  type        = bool
  default     = false
  nullable    = false
}

variable "iam_policy_path" {
  description = "Path in which to create the policies."
  type        = string
  default     = "/"
  nullable    = false
}

variable "iam_policy_tags" {
  description = "Tags specific to the IAM policies to create if toggle `var.iam_policy_create` is `true`. Will be merged `var.tags`."
  type        = map(string)
  default     = {}
  nullable    = false
}

variable "iam_policy_source_policy_documents" {
  description = "JSON to concatenate to create the external policies."
  type        = list(string)
  default     = []
  nullable    = false
}

variable "iam_policy_name_template" {
  type        = string
  description = "Template for the name of the policies to attach to the given IAM entities in `var.iam_policy_entity_arns`. “%s” must be somewhere inside the variable and will be replaced by the scope of the policy (`backup`, `ro`, `rw`, `rwd` or `full`)."
  default     = null

  validation {
    condition     = var.iam_policy_name_template == null || (can(regex("^[a-zA-Z0-9+=,\\.\\%/@-]+$", var.iam_policy_name_template)) && can(regex("\\%s", var.iam_policy_name_template)))
    error_message = "“var.iam_policy_name_template” is invalid."
  }
}

variable "iam_policy_description" {
  type        = string
  description = "Description of the external policy to attach to the given principals."
  default     = ""
  nullable    = false

  validation {
    condition     = 0 <= length(var.iam_policy_description) && length(var.iam_policy_description) <= 1024
    error_message = "“var.iam_policy_description” is invalid. Check the requirements in the variables.tf file."
  }
}

variable "iam_policy_export_json" {
  description = "Whether or not to export IAM policies as JSON."
  type        = bool
  default     = false
  nullable    = false
}

variable "iam_policy_export_actions" {
  description = "Whether or not to export IAM policies actions. A lightweight way to generate your own policies."
  type        = bool
  default     = false
  nullable    = false
}

variable "iam_policy_sid_prefix" {
  description = "Use a prefix for all `Sid` of all the policies created by this module."
  type        = string
  default     = ""
  nullable    = false
}

variable "iam_policy_external_resources_enabled" {
  description = "When `true`, user of this module is expected to set `var.iam_policy_external_resources_arns` with non null values."
  type        = bool
  default     = false
  nullable    = false
}

variable "iam_policy_external_resources_arns" {
  description = "ARNs of resources created outside this module to compose the external policies with. Along with internal resources created inside this module, the given ARNs will be merged into external IAM policies created by this module. Keys are free values."
  type        = map(string)
  default     = {}
  nullable    = false
}

variable "iam_policy_source_arns" {
  description = "Restrict access to the key to the given ARN sources. Wildcards are allowed. Allowed keys are `backup`, `ro`, `rw`, `rwd` or `full`, each specifying the expected level of access for entities inside."
  type        = map(map(string))
  default     = {}
  nullable    = false

  validation {
    condition = ((!contains([
      for key, arns in var.iam_policy_source_arns :
      (
        contains(["backup", "ro", "rw", "rwd", "audit", "full"], key) &&
        (
          !contains([
            for arn in values(arns) :
            (
              arn == null || can(regex("^arn:aws(-us-gov|-cn)?:[a-z]+:(([a-z]{2}-[a-z]{4,10}-[1-9]{1})?|\\*):([0-9]{12}|aws|\\*):.+/[a-zA-Z0-9+=,_:\\*\\./@{}'\\$\\?-]+$", arn))
            )
          ], false)
        )
      )
    ], false)))
    error_message = "One or more “var.iam_policy_source_arns” are invalid."
  }
}

variable "iam_policy_entity_arns" {
  description = "Restrict access to the key to the given IAM entities (roles or users). Wildcards are allowed. Allowed keys are `backup`, `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the permission scope for entities inside them. If the same entity is inside multiple scope, the least privileged scope takes precedence over the most privileged."
  type        = map(map(string))
  default     = {}
  nullable    = false

  validation {
    condition = (!contains([
      for key, arns in var.iam_policy_entity_arns :
      (
        contains(["backup", "ro", "rw", "rwd", "audit", "full"], key) &&
        (
          !contains([
            for arn in values(arns) :
            (
              arn == null || can(regex("^arn:aws(-us-gov|-cn)?:(iam|sts):(([a-z]{2}-[a-z]{4,10}-[1-9]{1})?|\\*):([0-9]{12}|aws|\\*):(assumed-role|role|user|federated-user)/[a-zA-Z0-9+=,_:\\*\\./@{}'\\$\\?-]+$", arn))
            )
          ], false)
        )
      )
    ], false))
    error_message = "One or more “var.iam_policy_entity_arns” are invalid."
  }
}

variable "iam_policy_restrict_by_account_ids" {
  description = "Restrict resources created by this module by the given account IDs. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains accounts wildcards."
  type        = list(string)
  default     = []
  nullable    = false

  validation {
    condition = ((!contains([
      for id in var.iam_policy_restrict_by_account_ids :
      (
        (can(regex("^[0-9]{12}$", id)))
      )
    ], false)))
    error_message = "One or more “var.iam_policy_restrict_by_account_ids” are invalid."
  }
}

variable "iam_policy_internal" {
  description = "A valid EFS policy JSON document to be applied to the EFS. Careful, this comes in complement of IAM entities passed to this module."
  type        = string
  default     = "{}"
  nullable    = false

  validation {
    condition     = can(jsondecode(var.iam_policy_internal))
    error_message = "“var.iam_policy_internal” must be a valid JSON string."
  }
}

locals {
  iam_should_generate_policy           = var.iam_policy_create
  iam_should_generate_policy_documents = local.iam_should_generate_policy || var.iam_policy_export_json

  iam_entities_all_scopes = toset(distinct(concat(keys(local.iam_policy_entity_scoped_arns), keys(var.iam_policy_source_arns))))

  iam_policy_source_arns = {
    for scope in keys(var.iam_policy_source_arns) :
    scope => values(var.iam_policy_source_arns[scope])
  }
  iam_policy_source_all_arns = flatten([
    for scope, source_arns in local.iam_policy_source_arns :
    source_arns
  ])

  iam_policy_entity_scoped_arns = {
    for scope in distinct(keys(var.iam_policy_entity_arns)) :
    scope => merge(lookup(var.iam_policy_entity_arns, scope, {}))
  }
  iam_policy_entity_all_arns = flatten([
    for scope, entities in local.iam_policy_entity_scoped_arns :
    values(entities)
  ])
}

#####
# IAM policy
#####

resource "aws_iam_policy" "this" {
  for_each = local.iam_should_generate_policy ? local.iam_entities_all_scopes : []

  name   = format(var.iam_policy_name_template, each.value)
  path   = var.iam_policy_path
  policy = data.aws_iam_policy_document.external[each.key].json

  description = var.iam_policy_description

  tags = merge(
    {
      Name        = format(var.iam_policy_name_template, each.value)
      Description = var.iam_policy_description
    },
    local.tags,
    var.iam_policy_tags,
  )
}

data "aws_iam_policy_document" "external" {
  for_each = local.iam_should_generate_policy_documents ? local.iam_entities_all_scopes : []

  source_policy_documents = var.iam_policy_source_policy_documents

  statement {
    sid     = "${var.iam_policy_sid_prefix}${upper(each.value)}"
    effect  = "Allow"
    actions = local.iam_scoped_actions[each.value]
    resources = compact(concat(
      [aws_efs_file_system.this.arn],
      var.iam_policy_external_resources_enabled ? values(var.iam_policy_external_resources_arns) : [null]
    ))
  }

  lifecycle {
    precondition {
      condition     = !var.iam_policy_external_resources_enabled || (var.iam_policy_external_resources_enabled && length(var.iam_policy_external_resources_arns) > 1)
      error_message = "“var.iam_policy_external_resources_enabled” is true, but “var.iam_policy_external_resources_arns” is empty."
    }
  }
}

output "iam_policy_jsons" {
  value = var.iam_policy_export_json ? { for scope, document in data.aws_iam_policy_document.external :
    scope => document.json
  } : null
}

output "iam_policy_actions" {
  value = var.iam_policy_export_actions ? local.iam_scoped_actions : null
}

output "iam_policies" {
  value = local.iam_should_generate_policy ? { for scope, policy in aws_iam_policy.this :
    scope => {
      arn       = policy.arn
      id        = policy.id
      name      = policy.name
      policy_id = policy.policy_id
    }
  } : null
}
